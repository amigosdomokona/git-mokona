/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anime.amigos.mokona;

import anime.amigos.mokona.comandos.Comando;
import anime.amigos.mokona.comandos.ComandoHelp;
import anime.amigos.mokona.comandos.ComandoRssShakaw;
import java.util.HashMap;
import java.util.Map;
import net.dv8tion.jda.api.entities.Message;

/**
 *
 * @author Rodrigo
 */

/*
  Esta classe define um bot.
  No discord todo bot precisa de um token,
  por isso o construtor desta classe tem String token.
  metodos, respostas e textoLido são alguns outros atributos de bot,
  esses começam com valores iniciais, pois receberão valor depois.
  Como o nosso bot será um personagem ele permanece abstrato,
  pois aqui ainda não dissemos qual personagem será.
  Ao ter implements InterfacePersonagem automáticamente
  tem os métodos abstratos: int motivacao(); e int motivacaoNecessaria();
  mesmo que não estejam escritos aqui.
 */
public abstract class Bot implements InterfacePersonagem {

  //Private: Apenas esta classe enxerga
  private String token;

  //Protected: Apenas esta classe e as classes que estendem desta enxergam
  protected HashMap<String, String> metodos;
  protected HashMap<String, String> respostas;
  protected String textoLido;
  protected String colegaIdentificado;
  protected String palavraChaveLida;
  protected String palavraAposPalavraChave;
  protected final Map<String, Comando> comandos;

  public Bot(String token) {
    this.token = token;
    metodos = new HashMap<>();
    respostas = new HashMap<>();
    textoLido = "";
    colegaIdentificado = "";
    palavraChaveLida = "";
    palavraAposPalavraChave = "";
    comandos = new HashMap<>();
    comandos.put("/help", new ComandoHelp());
    comandos.put("/novidades", new ComandoRssShakaw());
  }

  public final void analisarMensagem(Message mensagem) {
    textoLido = mensagem.getContentStripped();
    colegaIdentificado = mensagem.getAuthor().getId();
    if (textoLido.length() > 1
    && (textoLido.trim().charAt(0) == '/'
    || textoLido.trim().charAt(1) == '/')) {
      int inicioDaSegundaPalavra = textoLido.indexOf(" ") + 1;
      if (inicioDaSegundaPalavra != 0) {
        palavraChaveLida = textoLido.substring(0, inicioDaSegundaPalavra - 1);
      } else {
        palavraChaveLida = textoLido.substring(0, textoLido.length());
        palavraAposPalavraChave = "";
        return;
      }
      int inicioDaTerceiraPalavra = textoLido.indexOf(" ", inicioDaSegundaPalavra) + 1;
      if (inicioDaTerceiraPalavra != 0) {
        palavraAposPalavraChave = textoLido.substring(inicioDaSegundaPalavra, inicioDaTerceiraPalavra - 1);
      } else {
        palavraAposPalavraChave = textoLido.substring(inicioDaSegundaPalavra, textoLido.length());
        return;
      }
    } else {
      palavraChaveLida = "";
    }
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public HashMap<String, String> getMetodos() {
    return metodos;
  }

  public void setMetodos(HashMap<String, String> metodos) {
    this.metodos = metodos;
  }

  public HashMap<String, String> getRespostas() {
    return respostas;
  }

  public void setRespostas(HashMap<String, String> respostas) {
    this.respostas = respostas;
  }

  public String getTextoLido() {
    return textoLido;
  }

  public String getColegaIdentificado() {
    return colegaIdentificado;
  }

  public String getPalavraChaveLida() {
    return palavraChaveLida;
  }

  public String getPalavraAposPalavraChave() {
    return palavraAposPalavraChave;
  }
}
