package anime.amigos.mokona.comandos;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;

/**
 * Comando pra exibir as novidades via RSS do tracker ShaKaw.
 *
 * @author kurokami
 * @author shuguki
 */
public class ComandoRssShakaw implements Comando {

  private final String USER_AGENT = "Mozilla/5.0"; // qualquer coisa, só pra fingir que não é um robô
  private final String URL = "http://tracker.shakaw.com.br/rss_divulgacao_no_discord_xml.php?chave_secreta=gj545yp94ph9mej4hrn0gv4b53keog45098h490gi94j5g$09jer";
  private final String RE_GERAL = "<item>(.*?)</item>";
  private final String TAG_TITULO = "<title>";
  private final String TAG_TITULO_FECHAMENTO = "</title>";
  private final String TAG_IMAGEM = "<image>";
  private final String TAG_IMAGEM_FECHAMENTO = "</image>";
  private final String TAG_DESCRICAO = "<description>";
  private final String TAG_DESCRICAO_FECHAMENTO = "</description>";
  private final String TAG_LINK = "<link>";
  private final String TAG_LINK_FECHAMENTO = "</link>";
  private final String BB_CODE_IMAGEM = "[img=";
  private final String BB_CODE_IMAGEM_ABERTURA = "[img]";
  private final String BB_CODE_IMAGEM_FECHAMENTO = "[/img]";
  private final String MSG_ERRO = "Use `/novidades [N]` para ver os últimos **N** lançamentos do ShaKaw. **N** deve ser um número entre 1 e 15";

  private final Integer LIMITE_DE_CARACTERES_DO_DISCORD_PARA_DESCRICAO = 2048;
  private final String PALAVRA_PARA_IMAGEM = "\n\n**Imagem:**";

  private String imagemDeDentroDaDescricao;
  private int qtdeResultados;
  private boolean lancamentoAlvo = false;

  @Override
  public List<Message> responde(boolean nsfw, String... parametros) {
    // Valida o parâmetro
    if (parametros == null || parametros.length == 0) {
      qtdeResultados = 3;
    } else if (parametros.length > 1) {
      List<Message> mensagens = new ArrayList<>(1);
      mensagens.add(new MessageBuilder().append(MSG_ERRO).build());
      return mensagens;
    } else {
      if (parametros[0].endsWith("!")) {
        lancamentoAlvo = true;
        parametros[0] = parametros[0].substring(0, parametros[0].length() - 1);
        System.out.println(parametros[0]);
      } else {
        lancamentoAlvo = false;
      }
      try {
        qtdeResultados = Integer.parseInt(parametros[0]);

        if ((qtdeResultados < 1) || (qtdeResultados > 15)) {
          List<Message> mensagens = new ArrayList<>(1);
          mensagens.add(new MessageBuilder().append(MSG_ERRO).build());
          return mensagens;
        }
      } catch (NumberFormatException e) {
        List<Message> mensagens = new ArrayList<>(1);
        mensagens.add(new MessageBuilder().append(MSG_ERRO).build());
        return mensagens;
      }
    }

    // Executa o comando
    try {
      return parseRssXml(getRssXml(), nsfw);
    } catch (Exception e) {
      e.printStackTrace();
      List<Message> mensagens = new ArrayList<>(1);
      mensagens.add(new MessageBuilder().append("Erro ao acessar RSS do ShaKaw").build());
      return mensagens;
    }
  }

  private String getRssXml() throws Exception {
    // monta requisição HTTP
    HttpURLConnection conexaoHttp = (HttpURLConnection) (new URL(URL)).openConnection();
    conexaoHttp.setRequestMethod("GET");
    conexaoHttp.setRequestProperty("User-Agent", USER_AGENT);

    // lê a resposta
    StringBuilder respostaHttp = new StringBuilder();
    try (BufferedReader input = new BufferedReader(
    new InputStreamReader(conexaoHttp.getInputStream(), "UTF8"))) {
      String inputLine;
      while ((inputLine = input.readLine()) != null) {
        respostaHttp.append(inputLine).append("\n");
      }
    }

    // retorna o conteúdo bruto da resposta (rss.xml)
    return respostaHttp.toString();
  }

  private List<Message> parseRssXml(String rssXml, boolean nsfw) {
    Pattern patternGeral = Pattern.compile(RE_GERAL, Pattern.DOTALL);
    Matcher matcherGeral = patternGeral.matcher(rssXml);

    List<Message> respostas = new ArrayList();

    for (int i = 0; (i < qtdeResultados) && matcherGeral.find(); i++) {
      if (lancamentoAlvo && i < qtdeResultados - 1) {
        continue;
      }
      imagemDeDentroDaDescricao = "";

      String item = matcherGeral.group(1);
      int posicaoInicialDoTitulo = item.indexOf(TAG_TITULO) + TAG_TITULO.length();
      int posicaoFinalDoTitulo = item.indexOf(TAG_TITULO_FECHAMENTO);
      int posicaoInicialDaImagem = item.indexOf(TAG_IMAGEM) + TAG_IMAGEM.length();
      int posicaoFinalDaImagem = item.indexOf(TAG_IMAGEM_FECHAMENTO);
      int posicaoInicialDaDescricao = item.indexOf(TAG_DESCRICAO) + TAG_DESCRICAO.length();
      int posicaoFinalDaDescricao = item.indexOf(TAG_DESCRICAO_FECHAMENTO);
      int posicaoInicialDoLink = item.indexOf(TAG_LINK) + TAG_LINK.length();
      int posicaoFinalDoLink = item.indexOf(TAG_LINK_FECHAMENTO);

      String titulo = item.substring(posicaoInicialDoTitulo, posicaoFinalDoTitulo);

      boolean remover_conteudo_nsfw = false;
      if (!nsfw && titulo.endsWith(" (18+)")) {
        remover_conteudo_nsfw = true;
      }

      String imagemSeparada = item.substring(posicaoInicialDaImagem, posicaoFinalDaImagem);
      String descricao = item.substring(posicaoInicialDaDescricao, posicaoFinalDaDescricao);
      String url = item.substring(posicaoInicialDoLink, posicaoFinalDoLink);

      if (remover_conteudo_nsfw) {
        descricao = "A descrição deste torrent não pode ser exibida neste canal.";
      } else {
        int posicaoInicialDaImagemInterna = descricao.indexOf(BB_CODE_IMAGEM_ABERTURA);
        int posicaoFinalDaImagemInterna = -1;
        if (posicaoInicialDaImagemInterna > -1) {
          posicaoInicialDaImagemInterna = posicaoInicialDaImagemInterna + BB_CODE_IMAGEM_ABERTURA.length();
          posicaoFinalDaImagemInterna = descricao.indexOf(BB_CODE_IMAGEM_FECHAMENTO);
        }
        if (posicaoFinalDaImagemInterna > -1) {
          imagemDeDentroDaDescricao = descricao.substring(posicaoInicialDaImagemInterna, posicaoFinalDaImagemInterna);
        }

        if (imagemDeDentroDaDescricao.isEmpty()) {
          posicaoInicialDaImagemInterna = descricao.indexOf(BB_CODE_IMAGEM);
          posicaoFinalDaImagemInterna = -1;
          if (posicaoInicialDaImagemInterna > -1) {
            posicaoInicialDaImagemInterna = posicaoInicialDaImagemInterna + BB_CODE_IMAGEM.length();
            posicaoFinalDaImagemInterna = descricao.substring(posicaoInicialDaImagemInterna).indexOf("]");
          }
          if (posicaoFinalDaImagemInterna > -1) {
            imagemDeDentroDaDescricao = descricao.substring(posicaoInicialDaImagemInterna, posicaoFinalDaImagemInterna);
          }
        }

        descricao = formataDescricao(descricao);
      }

      EmbedBuilder embedBuilder = new EmbedBuilder();
      embedBuilder.setTitle(titulo, url);
      if (!remover_conteudo_nsfw) {
        if (!imagemSeparada.isEmpty()) {
          embedBuilder.setImage(imagemSeparada);
          descricao = truncaDescricao(descricao, true);
          embedBuilder.setDescription(descricao += PALAVRA_PARA_IMAGEM);
        } else if (!imagemDeDentroDaDescricao.isEmpty()) {
          try {
            embedBuilder.setImage(imagemDeDentroDaDescricao);
            descricao = truncaDescricao(descricao, true);
            embedBuilder.setDescription(descricao += PALAVRA_PARA_IMAGEM);
          } catch (Exception e) {
            descricao = truncaDescricao(descricao, false);
            embedBuilder.setDescription(descricao);
            e.printStackTrace();
          }
        }
      }else{
        embedBuilder.setDescription(descricao);
      }
      respostas.add(new MessageBuilder().setEmbed(embedBuilder.build()).build());
    }

    return respostas;
  }

  private String formataDescricao(String descricao) {
    descricao = descricao.replaceAll("(\\[\\*\\]|\\*)", "-")
    .replaceAll("\\[/?b\\]", "**")
    .replaceAll("(\\[/?i\\]|\\[/?o\\])", "*")
    .replaceAll("\\[/?u\\]", "__")
    .replaceAll("\\[/?sub\\]", "__")
    .replaceAll("&quot;", "\"")
    .replaceAll("\\[video=(.*?)\\]", "$1")
    .replaceAll("\\[img=(.*?)\\]", "$1")
    .replaceAll("\\[img\\](.*?)\\[/img\\]", "$1")
    .replaceAll("\\[url\\](.*?)\\[/url\\]", "$1")
    .replaceAll("\\[url=(.*?)\\](.*?)\\[/url\\]", "[$2]($1)")
    .replaceAll("\\[torrent=(\\d+)\\]", "https://tracker.shakaw.com.br/torrent.php?torrent_id=$1")
    .replaceAll("\\[topic=(\\d+)\\]", "https://tracker.shakaw.com.br/topico.php?topico_id=$1")
    .replaceAll("(\\[size=\\d+\\]|\\[/size\\])", "")
    .replaceAll("(\\[t\\d+\\]|\\[/t\\])", "")
    .replaceAll("(\\[quote=(.*?)\\]|\\[quote\\]|\\[/quote\\])", "")
    .replaceAll("(\\[color=(#{0,1}[0-9a-zA-Z]+)\\]|\\[/color\\])", "")
    .replaceAll("(\\[cor=(.*?)\\]|\\[/cor\\])", "")
    .replaceAll("(\\[font=(.*?)\\]|\\[/font\\])", "")
    .replaceAll("(\\[justify\\]|\\[left\\]|\\[center\\]|\\[right\\])", "")
    .replaceAll("(\\[hide\\]|\\[/hide\\]|\\[spoiler\\]|\\[/spoiler\\])", "")
    .replaceAll("(\\[ris\\]|\\[sob\\]|\\[/ris\\]|\\[/sob\\])", "");

    /* Se tiver uma URL de imagem no início da descrição, remove a URL: */
    if (descricao.indexOf(imagemDeDentroDaDescricao) <= descricao.length() * 0.25) {
      descricao = descricao.replaceFirst(Pattern.quote(imagemDeDentroDaDescricao), "");
    }

    return descricao;
  }

  //Método que trunca a descrição para evitar erro do discord.
  private String truncaDescricao(String descricao, boolean comImagem) {
    int quantidadePermitidaDeCaracteres = LIMITE_DE_CARACTERES_DO_DISCORD_PARA_DESCRICAO;

    if (comImagem) {
      quantidadePermitidaDeCaracteres -= PALAVRA_PARA_IMAGEM.length();
    }

    if (descricao.length() > quantidadePermitidaDeCaracteres) {
      descricao = descricao.substring(0, quantidadePermitidaDeCaracteres);
    }

    return descricao;
  }
}
