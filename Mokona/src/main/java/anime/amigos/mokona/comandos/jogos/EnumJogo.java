/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anime.amigos.mokona.comandos.jogos;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Rodrigo
 */
public enum EnumJogo {

  SHIRITORI("/shiritori", "/sh"),
  INVALIDO("");

  private final ArrayList<String> palavraChave;

  EnumJogo(String... palavraChave) {
    this.palavraChave = new ArrayList<>(Arrays.asList(palavraChave));
  }

  public static EnumJogo obterJogoPelaPalavraChave(String palavraChave) {
    for (EnumJogo jogo : values()){
      if (jogo.palavraChave.contains(palavraChave)){
        return jogo;
      }
    }
    return INVALIDO;
  }
  
  public String nomeDoJogo(){
    return palavraChave.get(0).substring(1,2).toUpperCase()+palavraChave.get(0).substring(2);
  }
  
  public String palavraChave(){
    return palavraChave.get(0);
  }
  
  @Override
  public String toString(){
    return "Enum: "+name()+"\nJogo: "+nomeDoJogo()+"\nPalavra chave: "+palavraChave;
  }
}
