/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anime.amigos.mokona;

/**
 *
 * @author Rodrigo
 */

/*
  Isso é uma interface e serve apenas para indicar que para ser um 
  personagem precisa ter determinados métodos. A classe que implementar
  esta interface terá que ter os métodos ou terá que ser uma classe
  abstrata (que não pode ser instanciada).
*/
public interface InterfacePersonagem {
  //Para ser um personagem precisa implementar os métodos abaixo:
  int motivacao();
  int motivacaoNecessaria();
}
