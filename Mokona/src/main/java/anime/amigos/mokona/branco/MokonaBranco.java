/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anime.amigos.mokona.branco;

import anime.amigos.mokona.Bot;
import anime.amigos.mokona.Compreensao;
import anime.amigos.mokona.comandos.Comando;
import anime.amigos.mokona.comandos.ComandoHelp;
import anime.amigos.mokona.comandos.ComandoRssShakaw;
import anime.amigos.mokona.comandos.jogos.EnumJogo;
import anime.amigos.mokona.comandos.jogos.shiritori.Shiritori;
import java.util.ArrayList;
import java.util.HashMap;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

/**
 *
 * @author Rodrigo
 */

/*
  Essa classe é o Mokona Branco, que é um personagem e também nosso bot.
  Essa classe extends Bot, ou seja, MokonaBranco é um tipo de Bot.
  Para que esta classe não fique abstrata,
  é necessário a implementação dos métodos abstratos herdados de Bot:
  motivacao() e motivacaoNecessaria() 
 */
public class MokonaBranco extends Bot {

  public MokonaBranco(String token) {
    super(token);
    comandos.put("c/help", new ComandoHelp());
    comandos.put("c/novidades", new ComandoRssShakaw());
  }

  @Override
  public int motivacao() {
    // Depois implementamos algo melhor para motivação
    return 1;
  }

  @Override
  public int motivacaoNecessaria() {
    // Depois implementamos algo melhor para motivacaoNecessaria
    return 1;
  }

  //Comportamento é uma classe Interna.
  //Criei ela internamente, para que este Comportamento seja
  //usado apenas para o MokonaBranco, isso deixa o código organizado.
  class Comportamento extends ListenerAdapter { //ListenerAdapter é do JDA

    //onMessageReceived é quando alguém envia uma mensagem no chat
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
      //Abaixo, é possível ver os detalhes do uso do JDA
      //--------- Os objetos abaixo são do JDA ------------

      //Para mostrar no console em qual servidor o Mokona está escutando
      System.out.println(event.getGuild().toString());

      //Obter a mensagem e o canal:
      Message mensagem = event.getMessage();
      MessageChannel canal = event.getChannel();
      //---------------------------------------------------

      MokonaBranco.this.analisarMensagem(mensagem);

      //Coloque o identificador do seu MokonaPreto.
      if (MokonaBranco.this.colegaIdentificado.equals("363505178479034373")) {
        if (MokonaBranco.this.palavraChaveLida.equals("/shiritori")) {
          Shiritori shiritori = Shiritori.getInstancia();
          if (MokonaBranco.this.palavraAposPalavraChave.equals("reset")) {
            shiritori.resetar();
            canal.sendMessage("Mokona vai reconsiderar as palavras que já foram escritas.").queue();
          } else {
            String resposta = shiritori.identificarFimDeJogoDoOutroMokona(palavraAposPalavraChave);
            if (resposta != null) {
              canal.sendMessage(resposta).queue();
            } else {
              shiritori.atualizarInformacoes(MokonaBranco.this.palavraAposPalavraChave);
            }
          }
        } else if (mensagem.getContentStripped().startsWith("Mokona não tem uma palavra que comece com ")) {
          //Caso o Mokona Preto não tenha uma palavra, então era para ter dado "Fim de Jogo". 
          //Porém não deu "Fim de Jogo", por causa das listas de palavras não estarem completamente 
          //adequadas ainda ou então por causa de alguma sílaba ainda não estar no método fimDeJogo.

          //Então, vou colocar para o Mokona Branco me chamar:
          canal.sendMessage("@shuguki, melhore as listas de palavras.").queue();
        }
      }

      //Para não responder outros bots, retorna.
      if (event.getAuthor().isBot()) {
        return;
      }

      boolean canalCorreto = true;
      if (event.getChannelType().toString().equals("TEXT")){
        EnumJogo jogo = EnumJogo.obterJogoPelaPalavraChave(MokonaBranco.this.palavraChaveLida);
        Comando comando = null;
        switch (jogo) {
          case SHIRITORI:
            if (canal.getName().equals("shiritori")) {
              comando = Shiritori.getInstancia();
            } else {
              canalCorreto = false;
              canal.sendMessage("Mokona só pode jogar shiritori no canal shiritori.").queue();
            }
            break;
          default:
            comando = comandos.get(MokonaBranco.this.palavraChaveLida);
        }
        if (canalCorreto && comando != null) {
          boolean nsfw = false;
          if (event.getTextChannel().isNSFW()) {
            nsfw = true;
          }
          for (Message msg : comando.responde(nsfw, MokonaBranco.this.palavraAposPalavraChave)) {
            canal.sendMessage(msg).queue();
          }
        }
      }

      //Abaixo é onde usamos o método compreender da classe Compreensao
      Compreensao compreesao = new Compreensao();
      ArrayList<String> interessesEncontrados = compreesao.compreender(MokonaBranco.this);

      //Com os interesses encontrados o MokonaBranco escreverá
      //suas respostas no canal. Repare que cada interesseEncontrado
      //é achave para acessar a reposta.
      HashMap<String, String> respostas = MokonaBranco.this.getRespostas();
      for (String interesseEncontrado : interessesEncontrados) {
        if (respostas.containsKey(interesseEncontrado)) {
          //Abaixo, a resposta é obtida com base no interesse(chave)
          //e enviada para o canal com o método sendMessage do JDA
          canal.sendMessage(respostas.get(interesseEncontrado)).queue();
        }
      }
    }
  }

  @Override
  public String toString() {
    return this.getToken();
  }
}
