package anime.amigos.mokona.comandos;

import net.dv8tion.jda.api.entities.Message;

/**
 * Interface que representa um comando executado pelo bot.
 *
 * O comando deve gerar mensagens de resposta para serem enviadas ao canal de origem.
 *
 * @author kurokami
 * @author shuguki
 */
public interface Comando {

  Iterable<Message> responde(boolean nsfw, String... parametros);

}
