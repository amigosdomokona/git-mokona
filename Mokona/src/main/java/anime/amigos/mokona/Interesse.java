/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anime.amigos.mokona;

import org.json.simple.JSONObject;

/**
 *
 * @author Rodrigo
 */

/*
  Esta classe define o objeto Interesse
  que possui três strings: frase, metodo e resposta.
  Acesse o arquivo Intersses.json e perceba que lá
  há vários objetos com frase, metodo e resposta.
  Com esta classe o que pegamos do arquivo
  Intersses.json pode ser representado e utilizado.
*/

public class Interesse {
  private String frase;
  private String metodo;
  private String resposta;

  public Interesse(JSONObject jsonObject){
    frase = (String)jsonObject.get("frase");
    metodo = (String)jsonObject.get("metodo");
    resposta = (String)jsonObject.get("resposta");
  }
  
  public String getFrase() {
    return frase;
  }

  public void setFrase(String frase) {
    this.frase = frase;
  }

  public String getMetodo() {
    return metodo;
  }

  public void setMetodo(String metodo) {
    this.metodo = metodo;
  }

  public String getResposta() {
    return resposta;
  }

  public void setResposta(String resposta) {
    this.resposta = resposta;
  }
}
