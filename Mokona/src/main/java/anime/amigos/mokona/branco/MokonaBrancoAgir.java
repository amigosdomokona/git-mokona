/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anime.amigos.mokona.branco;

import anime.amigos.mokona.Interesse;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.login.LoginException;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Rodrigo
 */

/*
  Esta é a classe que ao ser executada fará com que o bot funcione
  Para executar no NetBeans use o atalho shift F6.
 */
public class MokonaBrancoAgir {

  //Constante em Java é dito final
  private static final Integer QUANTIDADE_MAXIMA = 999;

  public static void main(String[] args) {
    String log4jConfPath = "src/main/resources/log4j.properties";
    PropertyConfigurator.configure(log4jConfPath);

    //Está com o token do meu Mokona, na sua branch modifique para o token do seu.
    MokonaBranco mokonaBranco = new MokonaBranco("MzUxOTAxMzUyNjcyMjMxNDI2.DIkvFg.Nck3mj7oUaJ5IKZHqzO7nvr03_4");
    HashMap<String, String> metodos = new HashMap<>();
    HashMap<String, String> respostas = new HashMap<>();
    JSONObject objetoJson;
    JSONParser parser = new JSONParser();
    try {
      //Os Interesses do MokonaBranco estão no arquivo Interesses.json
      objetoJson = (JSONObject) parser.parse(new FileReader("src/main/java/anime/amigos/mokona/branco/Interesses.json"));
      JSONObject objetoJsonInteresses = (JSONObject) objetoJson.get("interesses");
      Integer i = QUANTIDADE_MAXIMA;
      //O while abaixo vai iterar tudo, pois o objetivo
      //é utilizar o número também como prioridade.
      //O interesse de maior número é colocado primeiro.
      while (i > 0) {
        //No arquivo Interesses.json cada número é precedido de pu
        String puNumero = "pu" + i.toString();
        //O uso da classe Interesse serve para poder representar o que vem do arquivo
        JSONObject objetoJsonInteresse = (JSONObject) objetoJsonInteresses.get(puNumero);
        if (objetoJsonInteresse != null) {
          Interesse interesse = new Interesse(objetoJsonInteresse);
          metodos.put(interesse.getFrase(), interesse.getMetodo());
          respostas.put(interesse.getFrase(), interesse.getResposta());
        }
        i--;
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }

    //Agora daremos o que pegamos do arquivo
    //Interesses.json para o nosso MokonaBranco:
    mokonaBranco.setMetodos(metodos);
    mokonaBranco.setRespostas(respostas);

    //Como Comportamento é uma classe interna de MokonaBranco,
    //a sintax para instanciar é um pouco diferente:
    MokonaBranco.Comportamento comportamento = mokonaBranco.new Comportamento();

    //Aqui temos o JDA para que o bot possa funcionar:
    try {
      JDABuilder jda = JDABuilder.createDefault(mokonaBranco.getToken(), GatewayIntent.GUILD_MESSAGES);
      jda.addEventListeners(comportamento);
      jda.build();
    } catch (LoginException ex) {
      Logger.getLogger(MokonaBranco.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

}
