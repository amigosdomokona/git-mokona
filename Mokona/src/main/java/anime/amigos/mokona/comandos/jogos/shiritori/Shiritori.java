/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anime.amigos.mokona.comandos.jogos.shiritori;

import anime.amigos.mokona.comandos.Comando;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Rodrigo
 */
public class Shiritori implements Comando {

  private static Shiritori instancia;
  private HashMap<Character, List<String>> todasAsPalavras; //Letra Inicial e Lista de Palavras
  private HashMap<Character, List<String>> palavrasRestantes; //Letra Inicial e Lista de Palavras
  private HashMap<Character, List<String>> palavrasComuns; //Letra Inicial e Lista de Palavras
  private HashMap<Character, List<String>> palavrasComunsRestantes; //Letra Inicial e Lista de Palavras
  private ArrayList<String> silabasComCincoLetras;
  private ArrayList<String> silabasComQuatroLetras;
  private ArrayList<String> silabasComTresLetras;
  private ArrayList<String> silabasComDuasLetras;
  private ArrayList<String> silabasComUmaLetra;
  private ArrayList<ArrayList<String>> gruposDeSilabas;
  private String ultimaSilabaDaUltimaPalavraDoMokona;
  private int tentativa;
  private boolean jogoAcabou;
  private final String AVISO_FIM_DE_JOGO = ":regional_indicator_f: "
  + ":regional_indicator_i: :regional_indicator_m:   "
  + ":regional_indicator_d: :regional_indicator_e:   "
  + ":regional_indicator_j: :regional_indicator_o: "
  + ":regional_indicator_g: :regional_indicator_o:";

  private Shiritori() {
    todasAsPalavras = new HashMap<>();
    palavrasComuns = new HashMap<>();
    palavrasRestantes = new HashMap<>();
    palavrasComunsRestantes = new HashMap<>();
    silabasComCincoLetras = new ArrayList<>();
    silabasComQuatroLetras = new ArrayList<>();
    silabasComTresLetras = new ArrayList<>();
    silabasComDuasLetras = new ArrayList<>();
    silabasComUmaLetra = new ArrayList<>();
    gruposDeSilabas = new ArrayList<>();
    ultimaSilabaDaUltimaPalavraDoMokona = null;
    tentativa = 1;
    jogoAcabou = false;
    JSONParser parser = new JSONParser();
    JSONObject objetoJson;
    ArrayList<String> jsonArray;
    int indiceDoArray;
    try {
      objetoJson = (JSONObject) parser.parse(new FileReader("src/main/java/anime/amigos/mokona/comandos/jogos/lista_de_palavras_lingua_portuguesa.json"));
      for (Object letraChave : objetoJson.keySet()) {
        jsonArray = (JSONArray) objetoJson.get(letraChave);
        char letraInicial = letraChave.toString().charAt(0);
        todasAsPalavras.put(letraInicial, jsonArray);
        palavrasRestantes.put(letraInicial, (JSONArray) jsonArray.clone());
        palavrasComuns.put(letraInicial, (JSONArray) jsonArray.clone());
        palavrasComunsRestantes.put(letraInicial, (JSONArray) jsonArray.clone());
      }
      objetoJson = (JSONObject) parser.parse(new FileReader("src/main/java/anime/amigos/mokona/comandos/jogos/silabas.json"));
      jsonArray = (JSONArray) objetoJson.get("silabas_5_letras");
      indiceDoArray = 0;
      while (indiceDoArray < jsonArray.size()) {
        silabasComCincoLetras.add(jsonArray.get(indiceDoArray));
        indiceDoArray++;
      }
      jsonArray = (JSONArray) objetoJson.get("silabas_4_letras");
      indiceDoArray = 0;
      while (indiceDoArray < jsonArray.size()) {
        silabasComQuatroLetras.add(jsonArray.get(indiceDoArray));
        indiceDoArray++;
      }
      jsonArray = (JSONArray) objetoJson.get("silabas_3_letras");
      indiceDoArray = 0;
      while (indiceDoArray < jsonArray.size()) {
        silabasComTresLetras.add(jsonArray.get(indiceDoArray));
        indiceDoArray++;
      }
      jsonArray = (JSONArray) objetoJson.get("silabas_2_letras");
      indiceDoArray = 0;
      while (indiceDoArray < jsonArray.size()) {
        silabasComDuasLetras.add(jsonArray.get(indiceDoArray));
        indiceDoArray++;
      }
      silabasComUmaLetra.add("a");
      silabasComUmaLetra.add("e");
      silabasComUmaLetra.add("i");
      silabasComUmaLetra.add("o");
      silabasComUmaLetra.add("u");
    } catch (FileNotFoundException ex) {
      Logger.getLogger(Shiritori.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
      Logger.getLogger(Shiritori.class.getName()).log(Level.SEVERE, null, ex);
    } catch (ParseException ex) {
      Logger.getLogger(Shiritori.class.getName()).log(Level.SEVERE, null, ex);
    }
    gruposDeSilabas.add(silabasComCincoLetras);
    gruposDeSilabas.add(silabasComQuatroLetras);
    gruposDeSilabas.add(silabasComTresLetras);
    gruposDeSilabas.add(silabasComDuasLetras);
    gruposDeSilabas.add(silabasComUmaLetra);
  }

  public static Shiritori getInstancia() {
    if (instancia == null) {
      instancia = new Shiritori();
    }
    return instancia;
  }

  @Override
  public Iterable<Message> responde(boolean nsfw, String... parametros) {
    List<Message> retorno = new ArrayList<>(1);
    retorno.add(new MessageBuilder().append(escolherResposta(parametros[0])).build());
    return retorno;
  }

  public String jogarComOutroMokona(String palavraRecebida) {
    String silaba = identificarSilaba(palavraRecebida);
    return "/shiritori " + escolherPalavra(silaba);
  }

  public String identificarFimDeJogoDoOutroMokona(String palavraRecebida) {
    String silaba = identificarSilaba(palavraRecebida);
    fimDeJogo(silaba);
    if (jogoAcabou) {
      resetar();
      return AVISO_FIM_DE_JOGO + "\n\nQuem falou " + palavraRecebida
      + " perdeu.\nPuuuuuuu!";
    }
    return null;
  }

  public void atualizarInformacoes(String palavraRecebida) {
    if (!palavraRecebida.isEmpty()) {
      char letraDoInicio = palavraRecebida.toUpperCase().charAt(0);
      ArrayList<String> palavrasComunsPrimeiraLetraDaSilaba = (ArrayList) palavrasComunsRestantes.get(letraDoInicio);
      ArrayList<String> palavrasPrimeiraLetraDaSilaba = (ArrayList) palavrasRestantes.get(letraDoInicio);
      if (palavrasComunsPrimeiraLetraDaSilaba != null
      && palavrasPrimeiraLetraDaSilaba != null
      && palavrasComunsPrimeiraLetraDaSilaba.size() > 0
      && palavrasPrimeiraLetraDaSilaba.size() > 0) {
        palavrasComunsPrimeiraLetraDaSilaba.remove(palavraRecebida.toUpperCase());
        palavrasComunsRestantes.put(letraDoInicio, palavrasComunsPrimeiraLetraDaSilaba);
        palavrasPrimeiraLetraDaSilaba.remove(palavraRecebida.toUpperCase());
        palavrasRestantes.put(letraDoInicio, palavrasPrimeiraLetraDaSilaba);
      }
    }
    String silaba = identificarSilaba(palavraRecebida);
    ultimaSilabaDaUltimaPalavraDoMokona = silaba;
  }

  //Ainda vou colocar para retornar um boolean junto com a String,
  //ele representará sucesso(true) e falha(false).
  private String escolherResposta(String palavraRecebida) {
    if (palavraRecebida == null || palavraRecebida.isEmpty()) {
      return "Digite uma palavra após /shiritori ";
    } else if (palavraRecebida.equalsIgnoreCase("reset")) {
      resetar();
      return "Mokona vai reconsiderar as palavras que já foram escritas.";
    }
    palavraRecebida = substituirCaracteres(palavraRecebida.toLowerCase());
    if (ultimaSilabaDaUltimaPalavraDoMokona != null) {
      String silabaNecessaria = ultimaSilabaDaUltimaPalavraDoMokona.toLowerCase();
      if (!(palavraRecebida.startsWith(silabaNecessaria))) {
        return "A palavra que você disse não começa com \"" + silabaNecessaria + "\""
        + "\nÉ preciso dizer uma palavra que começa com \"" + silabaNecessaria + "\"";
      }
    }
    if (!averiguaDisponibilidadeDaPalavra(palavraRecebida)) {
      if (!averiguaExistenciaDaPalavra(palavraRecebida)) {
        return "A palavra que você escreveu não vale. :wink:";
      }
      return "Essa palavra já foi utilizada.";
    }
    String silaba = identificarSilaba(palavraRecebida);
    fimDeJogo(silaba);
    if (jogoAcabou) {
      resetar();
      return AVISO_FIM_DE_JOGO + "\n\nQuem falou " + palavraRecebida
      + " perdeu.\nPuuuuuuu!";
    }
    String resposta = "/shiritori " + escolherPalavra(silaba);
    if (jogoAcabou) {
      resetar();
      resposta += "\n\n" + AVISO_FIM_DE_JOGO + "\n\n" + "Vocês venceram!";
    }
    String comeco = silaba.isEmpty() ? palavraRecebida.substring(palavraRecebida.length() - 4) : silaba;
    String mensagem = "Mokona não tem uma palavra que comece com " + comeco;
    return resposta.equals("/shiritori ") ? mensagem : resposta;
  }

  private String substituirCaracteres(String palavra) {
    palavra = palavra.replace('à', 'a');
    palavra = palavra.replace('á', 'a');
    palavra = palavra.replace('â', 'a');
    palavra = palavra.replace('ã', 'a');
    palavra = palavra.replace('é', 'e');
    palavra = palavra.replace('ê', 'e');
    palavra = palavra.replace('í', 'i');
    palavra = palavra.replace('î', 'i');
    palavra = palavra.replace('ò', 'o');
    palavra = palavra.replace('ó', 'o');
    palavra = palavra.replace('ô', 'o');
    palavra = palavra.replace('ú', 'u');
    palavra = palavra.replace('ü', 'u');
    palavra = palavra.replace('ç', 'c');
    return palavra;
  }

  private String identificarSilaba(String palavra) {
    String silaba;
    int comprimento = palavra.length();
    for (int i = 5; i >= 1; i--) {
      silaba = palavra.substring(Math.max(comprimento - i, 0));
      switch (i) {
        case 5:
          if (silabasComCincoLetras.contains(silaba)) {
            return silaba;
          }
          break;
        case 4:
          if (silabasComQuatroLetras.contains(silaba)) {
            return silaba;
          }
          break;
        case 3:
          if (silabasComTresLetras.contains(silaba)) {
            return silaba;
          }
          break;
        case 2:
          if (silabasComDuasLetras.contains(silaba)) {
            return silaba;
          }
          break;
        case 1:
          if (silabasComUmaLetra.contains(silaba)) {
            return silaba;
          }
          break;
      }
    }
    return "";
  }

  private boolean averiguaDisponibilidadeDaPalavra(String palavra) {
    palavra = palavra.toUpperCase();
    char primeiroCaractere = palavra.charAt(0);
    ArrayList<String> lista = (ArrayList) palavrasRestantes.get(primeiroCaractere);
    if (lista != null && lista.contains(palavra)) {
      lista.remove(palavra);
      palavrasRestantes.put(primeiroCaractere, lista);
      ArrayList<String> lista2 = (ArrayList) palavrasComunsRestantes.get(primeiroCaractere);
      lista2.remove(palavra);
      palavrasComunsRestantes.put(primeiroCaractere, lista2);
      return true;
    }
    return false;
  }

  private boolean averiguaExistenciaDaPalavra(String palavra) {
    palavra = palavra.toUpperCase();
    char primeiroCaractere = palavra.charAt(0);
    List lista = todasAsPalavras.get(primeiroCaractere);
    return (lista != null && lista.contains(palavra));
  }

  //Talvez, no futuro, eu coloque: silaba, letraDoInicio,
  //palavrasComunsPrimeiraLetraDaSilaba e palavrasPrimeiraLetraDaSilaba
  //no método que chama escolherPalavra e junte o escolherPalavra e o
  //sorteiarPalavra em apenas um método que será recursivo.
  private String escolherPalavra(String silaba) {
    if (silaba.isEmpty()) {
      return "";
    }
    silaba = silaba.toUpperCase();
    char letraDoInicio = silaba.charAt(0);
    ArrayList<String> palavrasComunsPrimeiraLetraDaSilaba = (ArrayList) palavrasComunsRestantes.get(letraDoInicio);
    ArrayList<String> palavrasPrimeiraLetraDaSilaba = (ArrayList) palavrasRestantes.get(letraDoInicio);
    String palavraResposta;
    if (palavrasComunsPrimeiraLetraDaSilaba.size() > 0
    && !((palavraResposta = sortearPalavra(
    palavrasComunsPrimeiraLetraDaSilaba,
    silaba)).isEmpty())) {
    } else if (palavrasPrimeiraLetraDaSilaba.size() > 0
    && !((palavraResposta = sortearPalavra(
    palavrasPrimeiraLetraDaSilaba,
    silaba)).isEmpty())) {
    } else {
      return "";
    }
    if (palavraResposta == null) {
      return "";
    }
    palavrasComunsPrimeiraLetraDaSilaba.remove(palavraResposta.toUpperCase());
    palavrasComunsRestantes.put(letraDoInicio, palavrasComunsPrimeiraLetraDaSilaba);
    palavrasPrimeiraLetraDaSilaba.remove(palavraResposta.toUpperCase());
    palavrasRestantes.put(letraDoInicio, palavrasPrimeiraLetraDaSilaba);
    return palavraResposta;
  }

  private String sortearPalavra(ArrayList<String> listaDePalavras, String silaba) {
    ArrayList<String> palavrasPossiveis = new ArrayList<>();
    String palavraResposta = "";
    for (String palavra : listaDePalavras) {
      if (palavra.matches("^" + silaba + ".*")) {
        palavrasPossiveis.add(palavra);
      }
    }
    if (palavrasPossiveis.size() > 0) {
      int sorteada = (int) (Math.random() * palavrasPossiveis.size());
      palavraResposta = palavrasPossiveis.get(sorteada).toLowerCase();
      ultimaSilabaDaUltimaPalavraDoMokona = identificarSilaba(palavraResposta);
      fimDeJogo(ultimaSilabaDaUltimaPalavraDoMokona);
      if (jogoAcabou && tentativa < 11) {
        tentativa++;
        palavraResposta = sortearPalavra(listaDePalavras, silaba);
      } else {
        tentativa = 1;
      }
    }
    return palavraResposta;
  }

  //Temporariamente public
  public void resetar() {
    for (Character caractere : palavrasComuns.keySet()) {
      palavrasRestantes.get(caractere).removeAll(palavrasRestantes.get(caractere));
      palavrasRestantes.get(caractere).addAll(todasAsPalavras.get(caractere));
      palavrasComunsRestantes.get(caractere).removeAll(palavrasComunsRestantes.get(caractere));
      palavrasComunsRestantes.get(caractere).addAll(palavrasComuns.get(caractere));
    }
    ultimaSilabaDaUltimaPalavraDoMokona = null;
  }

  private void fimDeJogo(String silaba) {
    switch (silaba) {
      case "lhe":
      case "lhi":
      case "lho":
      case "lhu":
      case "lhar":
      case "lher":
      case "lhor":
      case "lhas":
      case "lhis":
      case "lhos":
      case "lhau":
      case "lhei":
      case "lhum":
      case "nha":
      case "nhe":
      case "nhi":
      case "nho":
      case "vre":
      case "vro":
      case "cao":
      case "dao":
      case "xo":
      case "rio":
      case "ria": //Só tem riacho
      case "sao": //Só tem são-paulino e são-paulina
      case "nal": //Só tem nalgum
      case "quaz":
      case "rax":
      case "gues":
      case "gao":
      case "noz": //Só tem noz-moscada
      case "das": //Só tem dasein
      case "nao":
      case "cher":
      case "vem":
      case "loz":
      case "rir": //Só tem rir
      case "dril":
      case "tao":
      case "lio":
      case "zem":
      case "cem": //Só tem cemitério e cem
      case "bres":
      case "by":
      case "zal":
      case "nus":
      case "iz":
      case "guim":
      case "nol":
      case "brar":
      case "tez":
      case "xim":
      case "nhar":
      case "jem":
      case "day":
      case "nil":
        jogoAcabou = true;
        return;
    }
    jogoAcabou = false;
  }

  public String getUltimaSilabaDaUltimaPalavraDoMokona() {
    return ultimaSilabaDaUltimaPalavraDoMokona;
  }

}
