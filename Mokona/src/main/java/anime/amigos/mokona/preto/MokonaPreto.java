/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anime.amigos.mokona.preto;

import anime.amigos.mokona.Bot;
import anime.amigos.mokona.Compreensao;
import anime.amigos.mokona.comandos.Comando;
import anime.amigos.mokona.comandos.ComandoHelp;
import anime.amigos.mokona.comandos.ComandoRssShakaw;
import anime.amigos.mokona.comandos.jogos.EnumJogo;
import anime.amigos.mokona.comandos.jogos.shiritori.Shiritori;
import java.util.ArrayList;
import java.util.HashMap;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

/**
 *
 * @author Rodrigo
 */
public class MokonaPreto extends Bot {

  private int motivacao;

  public MokonaPreto(String token) {
    super(token);
    motivacao = 1;
    comandos.remove("/help");
    comandos.remove("/novidades");
    comandos.put("y/help", new ComandoHelp());
    comandos.put("y/novidades", new ComandoRssShakaw());
  }

  @Override
  public int motivacao() {
    return motivacao;
  }

  @Override
  public int motivacaoNecessaria() {
    return 1;
  }

  class Comportamento extends ListenerAdapter {

    private ArrayList<String> ultimasInformacoesInteressantes = new ArrayList<>();

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
      motivacao = 1;
      System.out.println(event.getGuild().toString());

      Message mensagem = event.getMessage();
      MessageChannel canal = event.getChannel();
      MokonaPreto.this.analisarMensagem(mensagem);
      boolean colegaCorreto = false;

      Shiritori shiritori = Shiritori.getInstancia();
      //Coloque o identificador do seu MokonaBranco.
      if (MokonaPreto.this.colegaIdentificado.equals("351901352672231426")) {
        if (MokonaPreto.this.textoLido.startsWith("Mokona não tem uma palavra que comece com ")
        || MokonaPreto.this.textoLido.startsWith("@shuguki, melhore as listas de palavras.")) {
          canal.sendMessage("/shiritori reset").queue();
          return;
        }
        if (MokonaPreto.this.textoLido.equals("Essa palavra já foi utilizada.")) {
          shiritori.resetar();
          return;
        }
        colegaCorreto = true;
      }

      if (event.getChannelType().toString().equals("TEXT")) {
        EnumJogo jogo = EnumJogo.obterJogoPelaPalavraChave(MokonaPreto.this.palavraChaveLida);
        Comando comando = null;
        switch (jogo) {
          case SHIRITORI:
            //Coloque o identificador do seu Mokona Preto
            if (!MokonaPreto.this.colegaIdentificado.equals("363505178479034373")
            && !MokonaPreto.this.palavraAposPalavraChave.equals("reset")) {
              //Se não for o próprio Mokona Preto e não for reset
              //então chama o método atualizarInformacoes
              shiritori.atualizarInformacoes(MokonaPreto.this.palavraAposPalavraChave);
            }
            if (colegaCorreto) {
              String resposta = shiritori.jogarComOutroMokona(MokonaPreto.this.palavraAposPalavraChave);
              if (resposta.equals("/shiritori ")) {
                String finalDaPalavra = shiritori.getUltimaSilabaDaUltimaPalavraDoMokona();
                if (finalDaPalavra.isEmpty()) {
                  finalDaPalavra = palavraAposPalavraChave.substring(palavraAposPalavraChave.length() - 4);
                }
                canal.sendMessage("Mokona não tem uma palavra que comece com " + finalDaPalavra).queue();
              } else {
                canal.sendMessage(resposta).queue();
              }
              return;
            }
          default:
            comando = comandos.get(MokonaPreto.this.palavraChaveLida);
            if (comando != null) {
              boolean nsfw = false;
              if (event.getTextChannel().isNSFW()) {
                nsfw = true;
              }
              for (Message msg : comando.responde(nsfw, MokonaPreto.this.palavraAposPalavraChave)) {
                canal.sendMessage(msg).queue();
              }
            }
        }
        if (event.getAuthor().isBot() && !colegaCorreto) {
          motivacao--;
        }
      }

      Compreensao compreesao = new Compreensao();
      ArrayList<String> interessesEncontrados = compreesao.compreender(MokonaPreto.this);

      HashMap<String, String> respostas = MokonaPreto.this.getRespostas();

      for (String interesseEncontrado : interessesEncontrados) {
        if (respostas.containsKey(interesseEncontrado)) {
          if (ultimasInformacoesInteressantes.contains(interesseEncontrado)) {
            motivacao--;
          } else {
            ultimasInformacoesInteressantes.add(interesseEncontrado);
          }
          if (motivacao() >= motivacaoNecessaria()) {
            canal.sendMessage(respostas.get(interesseEncontrado)).queue();
          } else {
            ultimasInformacoesInteressantes = new ArrayList<>();
          }
        }
      }
    }
  }

  @Override
  public String toString() {
    return this.getToken();
  }
}
