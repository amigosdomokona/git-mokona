/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anime.amigos.mokona.preto;

import anime.amigos.mokona.Interesse;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.login.LoginException;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Rodrigo
 */
public class MokonaPretoAgir {

  private static final Integer QUANTIDADE_MAXIMA = 999;

  public static void main(String[] args) {
    String log4jConfPath = "src/main/resources/log4j.properties";
    PropertyConfigurator.configure(log4jConfPath);

    MokonaPreto mokonaPreto = new MokonaPreto("MzYzNTA1MTc4NDc5MDM0Mzcz.DLCMGQ.EpYowfbQp_WKBBoZaKS1yGDONlg");
    HashMap<String, String> metodos = new HashMap<>();
    HashMap<String, String> respostas = new HashMap<>();
    JSONObject objetoJson;
    JSONParser parser = new JSONParser();

    try {
      objetoJson = (JSONObject) parser.parse(new FileReader("src/main/java/anime/amigos/mokona/preto/Interesses.json"));
      JSONObject objetoJsonInteresses = (JSONObject) objetoJson.get("interesses");
      Integer i = QUANTIDADE_MAXIMA;

      while (i > 0) {
        String puNumero = "pu" + i.toString();

        JSONObject objetoJsonInteresse = (JSONObject) objetoJsonInteresses.get(puNumero);
        if (objetoJsonInteresse != null) {
          Interesse interesse = new Interesse(objetoJsonInteresse);
          metodos.put(interesse.getFrase(), interesse.getMetodo());
          respostas.put(interesse.getFrase(), interesse.getResposta());
        }
        i--;
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }

    mokonaPreto.setMetodos(metodos);
    mokonaPreto.setRespostas(respostas);

    MokonaPreto.Comportamento comportamento = mokonaPreto.new Comportamento();

    try {
      JDABuilder jda = JDABuilder.createDefault(mokonaPreto.getToken(), GatewayIntent.GUILD_MESSAGES);
      jda.addEventListeners(comportamento);
      jda.build();
    } catch (LoginException ex) {
      Logger.getLogger(MokonaPreto.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

}
