/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anime.amigos.mokona;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Rodrigo
 */

/*
  Esta classe possui diferentes métodos de compreensão.
  Esta classe serve para que um bot identifique de diferentes formas
  algo no qual possui interesse. Exemplo: Se o bot tem interesse
  em uma frase em que esteja apenas escrito o nome próprio Clamp
  e nada mais, então o método tem que ser apenasCom_E_ExatamenteIgual.
  Os interesses de um bot estão definidos em um arquivo json no pacote do bot.
  
  O método compreender pega cada interesse que o bot tem (na ordem da lista)
  e seu respectivo método para procurar o que o bot leu (bot.getTextoLido())
  Toda vez que o bot se interessa pelo o que leu, o interesse é
  colocado no ArrayList interessesEncontrados.
  Todos interessesEncontrados são retornados no final.
*/

public class Compreensao {

  public boolean noInicio_E_MaiusculoMinusculo(String estimulo, String leu) {
    boolean retorno = false;
    int quantidadeDeLetras = estimulo.length();
    if (leu.length() >= quantidadeDeLetras && leu.substring(0, quantidadeDeLetras).equalsIgnoreCase(estimulo)) {
      retorno = true;
    }
    return retorno;
  }

  public boolean qualquerLugar_E_MaiusculoMinusculo(String estimulo, String leu) {
    boolean retorno = false;
    int quantidadeDeLetras = estimulo.length();
    for (int i = 0; i <= leu.length() - quantidadeDeLetras; i++) {
      if (leu.substring(i, i + quantidadeDeLetras).equalsIgnoreCase(estimulo)) {
        retorno = true;
        break;
      }
    }
    return retorno;
  }

  public boolean apenasCom_E_MaiusculoMinusculo(String estimulo, String leu) {
    boolean retorno = false;
    if (estimulo.equalsIgnoreCase(leu)) {
      retorno = true;
    }
    return retorno;
  }

  public boolean apenasCom_E_ExatamenteIgual(String estimulo, String leu) {
    boolean retorno = false;
    if (estimulo.equals(leu)) {
      retorno = true;
    }
    return retorno;
  }

  public ArrayList<String> compreender(Bot bot) {
    HashMap<String, String> metodos = bot.getMetodos();
    String leu = bot.getTextoLido();
    ArrayList<String> interessesEncontrados = new ArrayList<>();
    if (bot.motivacao() < bot.motivacaoNecessaria()) {
      //Se não teve motivação então retorna o arrayList vazio
      return interessesEncontrados;
    }
    for (String interesse : metodos.keySet()) {
      String formaDeCompreensao = metodos.get(interesse);
      switch (formaDeCompreensao) {
        case "noInicio_E_MaiusculoMinusculo":
          if (noInicio_E_MaiusculoMinusculo(interesse, leu)) {
            interessesEncontrados.add(interesse);
          }
          break;
        case "qualquerLugar_E_MaiusculoMinusculo":
          if (qualquerLugar_E_MaiusculoMinusculo(interesse, leu)) {
            interessesEncontrados.add(interesse);
          }
          break;
        case "apenasCom_E_MaiusculoMinusculo":
          if (apenasCom_E_MaiusculoMinusculo(interesse, leu)) {
            interessesEncontrados.add(interesse);
          }
          break;
        case "apenasCom_E_ExatamenteIgual":
          if (apenasCom_E_ExatamenteIgual(interesse, leu)) {
            interessesEncontrados.add(interesse);
          }
          break;
        default:
          break;
      }
    }
    return interessesEncontrados;
  }
}
