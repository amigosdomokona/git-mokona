package anime.amigos.mokona.comandos;

import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;

/**
 * Comando pra exibir um help maroto.
 *
 * @author kurokami
 * @author shuguki
 */
public class ComandoHelp implements Comando {

  private final String[] comandos = new String[]{
    "`/help`: exibe esse help que você está vendo agora;",
    "`/novidades`: exibe as novidades do tracker ShaKaw;"
  };

  @Override
  public Iterable<Message> responde(boolean nsfw, String... parametros) {
    List<Message> mensagens = new ArrayList<>(1);
    mensagens.add(getResposta());
    return mensagens;
  }

  private Message getResposta() {
    StringBuilder resposta = new StringBuilder("**Comandos:**\n");
    for (String comando : comandos) {
      resposta.append(comando).append("\n");
    }
    return new MessageBuilder().append(resposta.toString()).build();
  }

}
